
import { createVuetify } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import 'vuetify/styles'

const ApSimDark = {
  dark: true,
  colors:{
    primary: '#3fb649',
    secondary: '#666666',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
    background: '#121212'
  }
}

const ApSimLight = {
  dark: false,
  colors:{
    primary: '#216e35',
    secondary: '#888888',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
    background: '#828282',
  }
}

const defaultTheme = 'ApSimDark'; 

//check system theme 
// if (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) {
//   defaultTheme = 'ApSimLight';
// } 

const vuetify = createVuetify({
    ssr: true,
    defaults:{
      VBtn: { variant: 'outlined' },
      VCard: { outlined: true },
    },
    icons: {
      defaultSet: 'mdi',
      aliases,
      sets: {
        mdi,
      }
    },
    theme: {

        defaultTheme: defaultTheme,
        themes:{
          ApSimDark,
          ApSimLight
        }
    },
    components,
    directives,
  })

export default vuetify;