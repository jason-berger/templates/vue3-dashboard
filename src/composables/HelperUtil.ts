
import { ref, onMounted } from 'vue';

export function isValidEmail(email: string) : boolean
{
    const emailRegex = /\S+@\S+\.\S+/;
    return emailRegex.test(email);
}
