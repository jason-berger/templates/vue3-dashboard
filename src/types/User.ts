
interface PermissionMap {
    [roleName: string]: string[];
  }

const _PERMISSIONS : PermissionMap = {
    'admin' : ['user:write', 'user:read', 'user:delete', 
               'platform:write','platform:read', 'platform:delete',
               'package:write', 'package:read', 'package:delete'],
    'maintainer' : [ 'platform:write','platform:read', 'platform:delete',
                     'package:write', 'package:read', 'package:delete',],
    'reporter' : [ 'platform:read', 'package:read',],
    'pending' : ['none'],
    'device' : [ 'device:log', 'package:read' , 'platform:read']
}

export class User  {

   first: string = '';
    last: string = '';
    username: string = '';
    email: string = '';
    role: string = '';
    permissions: string[] = [];
    _id: string = '';


    constructor( userResp?: any, config?: any) {


        if(userResp != undefined )
        {
            this._id = userResp._id;
            this.first = userResp.first;
            this.last = userResp.last;
            this.username = userResp.username;
            this.email = userResp.email;
            this.role = userResp.role;

            if(config != undefined)
            {
                this.permissions = config.roles.find((role: any) => role.name == this.role).permissions;
            }
            else 
            {
                this.permissions = _PERMISSIONS[this.role];

            }

        }

    }

    isAdmin(){
        return this.role == 'admin';
    }


    hasPermission(permission: string) {
        return this.permissions.includes(permission);
    }
    
}