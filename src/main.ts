import { createApp } from 'vue'
import { createPinia } from "pinia";
import App from './App.vue'



import vuetify from './plugins/vuetify';
import 'vuetify/styles'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import router from './router'



createApp(App).use(router).use(vuetify).use(createPinia()).mount('#app')
