
import { defineStore } from 'pinia'
import { get, post, restDelete, submitFormData, put, patch } from './common'

export const useUserStore = defineStore('user', {
    state: () => ({
        users: [],
        cuurentUser: {}
    }),
    getters: {
        getUsers(state) {
            return state.users;
        },

        getCurrentUser(state)  {
            return state.cuurentUser ;
        }
    },

    actions: {

        async fetchUsers() {
            const response = await get('/users');
            this.users = response.data;
        },

        async updateUser(user: any, newData: any) {
            const response = await patch(`/users/${user._id}`, newData);
            
            return response;
        },
        

      async signUpUser(user: any) {
        const response = await post('/users', user);
        return response;
      }
        

    },
})