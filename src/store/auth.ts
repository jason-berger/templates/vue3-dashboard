import { defineStore } from 'pinia'
import { get, post, restDelete, submitFormData } from './common'
import { User } from '../types/User'

let _TOKEN = '';


export function isLoggedIn() {
    return _TOKEN != '';
    
}

export function getToken() {
    return _TOKEN;
}

export function hasPermission(permission: string) 
{
    return true;
}



export const useAuthStore = defineStore('auth', {
    state: () => ({
        authUser : new User(),
        token: null,
        config: {},
        backups: []
    }),
    getters: {
        getAuth(state) {
            return state.authUser;
        },
        getConfig(state) {
            return state.config;
        },

    },

    actions: {

        async login(credentials: any) {
            const response = await post('/auth/login', credentials);
            
            if(response.status == 200 && response.data.user)
            {
                this.authUser = new User(response.data.user, this.config);
                this.token = response.data.token;
                _TOKEN = response.data.token;
            }

            
            return response;
        },

        async logout() {
            
            this.authUser = new User();
            this.token = null;
            _TOKEN = '';

        },
        async fetchConfig() {
            const response = await get('/configuration');
            this.config = response.data;
        },

        async fetchBackups() {
            const response = await get('/backups');
            this.backups = response.data;
        }


         
    },

})