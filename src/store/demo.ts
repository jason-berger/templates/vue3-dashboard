
import { defineStore } from 'pinia'
import { get, post, restDelete, submitFormData, put, patch } from './common'

export const useDemoStore = defineStore('demo', {
    state: () => ({
        demoString: 'Hello, World!',
        demoValue: 0,

    }),


    actions: {

        async updateValue() {
            //In a real-world application, this would be an API call
            // something like :
            // const response = await post(`/users/${user._id}`, newData);
            // this.demoValue = response.data.value;
            this.demoValue = this.demoValue + 1;
        },


        

    },
})