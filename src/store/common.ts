import axios from 'axios'
import { getToken } from './auth';


const protocol = window.location.protocol; // returns "http:" or "https:"
const server = window.location.hostname; // returns "192.168.10.140"

const api_url = `/api/v1`


export async function get(path: string) {

    const response = await axios.get(api_url + path,{
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    })

    return response
}

export async function post(path: string, data: any) {

    try
    {
        const response = await axios.post(api_url + path, data,{
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        });

        return response
    }
    catch(error : any)
    {
        return error.response;
    }


}


export async function patch(path: string, data: any)
{
    const response = await axios.patch(api_url + path, data,{
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    });
    return response;
}

export async function put(path: string, data: any)
{
    const response = await axios.put(api_url + path, data,{
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    });
    return response;
}

export async function restDelete(path: string)
{
    const response = await axios.delete(api_url + path,{
        headers: {
            'Authorization': `Bearer ${getToken()}`
        }
    });

    return response;
}

export async function submitFormData(path: string, data: any)
{
    const response = await axios.post(api_url + path, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${getToken()}`
        }
    });

    return response;
}

