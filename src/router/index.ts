import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import WorkBenchView from '@/views/WorkBenchView.vue'
import DemoView from '@/views/DemoView.vue'

import LoginView from '@/views/LoginView.vue'

import { isLoggedIn } from '@/store/auth'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    alias: '/demo',
    name: 'Demo',
    component: DemoView
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


//Uncomment this to enable auth
// router.beforeEach((to, from, next) => {
//   if (to.name !== 'Login' && !isLoggedIn()) 
//   {

//     next({ name: 'Login' })
  
//   } else {
//     next()
//   }
// })

export default router
