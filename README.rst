Vue3-Dashboard template 
=======================

This project is a template for a dashboard using Vue3 withe Vuetify. 

It will display the DemoView which provides example patterns for the following components:

    - DemoComponentCard: Shows an example of a reusable card component that can accept props 
    - DemoComponentDilaog: Shows an example of a reusable dialog component that can accept props, and emit events to update the value of props

Setup 
-----

.. code:: bash 

    npm install 

Run 
---

.. code:: bash 

    npm run serve


This will start a local instance of the app at `http://localhost:8080/`. When changes are made to source files, the app will automatically reload to reflect the changes.


.. note:: If you are using WSL, run `npm run serve` from inside the WSL file system. Running on the Windows file system will cause issues with file watching and the application will not automatically load changes 
