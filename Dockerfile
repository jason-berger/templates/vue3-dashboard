# Build stage
FROM node:16 as build-stage

ARG USE_EXISTING_BUILD=false

WORKDIR /app

COPY package*.json ./

RUN if [ "$USE_EXISTING_BUILD" = "true" ]; then \
    echo "Using existing build, skipping install"; \
  else \
    npm install; \
  fi


COPY . .

# Check the build argument and decide whether to build or use existing package
RUN if [ "$USE_EXISTING_BUILD" = "true" ]; then \
    echo "Using existing package"; \
  else \
    echo "Building package"; \
    npm run build; \
  fi

# Production stage
FROM nginx:stable-alpine as production-stage

COPY --from=build-stage /app/dist /var/www/html

RUN rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/templates/
COPY start-nginx.sh /app/start-nginx.sh


ENV API_HOST=api 
ENV API_PORT=3096

EXPOSE 8080

CMD ["/bin/sh", "/app/start-nginx.sh"]
